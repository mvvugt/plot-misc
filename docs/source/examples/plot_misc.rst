Examples
-------------------------------------------

Here is some sample templates illustrating 
the basic features of plot-misc. 

.. toctree::
   :maxdepth: 1

   plots/barcharts.nblink
   plots/forestplot.nblink
   plots/heatmap.nblink
   plots/incidencematrix.nblink
   plots/machine_learning.nblink
   plots/pychart.nblink
   plots/volcanoplot.nblink
